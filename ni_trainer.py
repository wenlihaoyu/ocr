# -*- coding: utf-8 -*-
import cv2
import numpy as np
import os
import collections
from string import ascii_lowercase
from utils import mosaic, Rect
from models import cvSVM, MLP, RTrees, Bayes
from features import MomentsFeature, BinaryFeature, HOGFeature


class OCR(object):
    def __init__(self, datatype, feature):
        self.image = None
        self.datatype = datatype
        self.segments = []
        self.feature = feature

    def segmentate(self, img):
        img_tmp = img.copy()
        contours, hierarchy = cv2.findContours(img_tmp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        if len(contours) > 0 and cv2.contourArea(contours[0]) > 0:
            [x, y, w, h] = cv2.boundingRect(contours[0])
            roi = img[y:y+h, x:x+w]
            newimg = cv2.resize(roi, (32, 32), interpolation=cv2.INTER_AREA)
            #cv2.imwrite("images/"+str(len(self.segments))+".png", newimg)
            self.segments.append(newimg)
        else:
            h, w = img.shape
            x, y = 0, 0
            roi = img_tmp[y:y+h, x:x+w]
            newimg = cv2.resize(roi, (32, 32), interpolation=cv2.INTER_AREA)
            self.segments.append(newimg)


class Training(OCR):
    def __init__(self, datatype, feature):
        super(Training, self).__init__(datatype, feature)

    def load_data(self, filename, cell_size):
        print("Loading %s"% filename)
        self.image = cv2.imread(filename, 0)
        ret, self.threshold = cv2.threshold(self.image, 100, 255, cv2.THRESH_BINARY_INV)
        self.data = self.divide(self.threshold, cell_size)

        nr = types[self.datatype]
        if self.datatype == datatypes[0]:
            k = np.arange(nr)
            self.labels = np.repeat(k, len(self.data)/nr)
        else:
            k = list(ascii_lowercase)
            self.labels = np.repeat(k, len(self.data)/nr)

    def shuffle(self):
        rand = np.random.RandomState(321)
        shuffle = rand.permutation(len(self.data))
        self.data, self.labels = self.data[shuffle], self.labels[shuffle]

    def divide(self, image, cell_size):
        height, width = image.shape
        sign_count = width // cell_size[0]
        number_of_rows = height // cell_size[1]
        cells = [np.hsplit(row, sign_count) for row in np.vsplit(image, number_of_rows)]
        cells = np.array(cells)
        cells = cells.reshape(-1, cell_size[1], cell_size[0])
        return cells

    def train_save(self, model, filename):
        self.samples_data = np.asarray(self.samples_data, dtype=np.float32)
        print("Training...")
        model.train(self.samples_data, self.labels_data)
        print("Training finished")
        model.save(filename)
        print "Saving output to ", filename

class Recognizing(OCR):
    def __init__(self, datatype, feature, im_filename, data_filename, model):
        super(Recognizing, self).__init__(datatype, feature)
        self.image = cv2.imread(im_filename, 0)
        ret, self.image = cv2.threshold(self.image, 100, 255, cv2.THRESH_BINARY_INV)
        if not os.path.exists(data_filename):
            print(data_filename + " not found!\n")
            return
        self.model = model
        self.model.load(data_filename)

    def test(self, test_data, labels_data, digits_data):
        result = self.predict(test_data)
        print(labels_data)
        err = (labels_data != result).mean()
        print 'error: %.2f %%' % (err*100)

        confusion = np.zeros((10, 10), np.int32)
        for i, j in zip(labels_data, result):
            confusion[i, j] += 1
        print('confusion matrix:')
        print(confusion)

        vis = []
        for img, flag in zip(digits_data, result == labels_data):
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            if not flag:
                img[...,:2] = 0
            vis.append(img)
        show(mosaic(25, vis), "test results")

    def predict(self, test_data):
        result = self.model.predict(test_data)
        result = result.astype(int)
        print(result)
        return result

    def process_input(self):
        # STRASZNY GUWNOKOD, posprzątać KONIECZNIE
        rows, cols = self.image.shape
        line = []
        words = []
        word = []
        spaces = []
        space = []
        rects = []
        letter_rects = []
        #dzielenie na słowa, znaki
        for i in xrange(rows):
            white = cv2.countNonZero(self.image[i, :])
            if white != 0:
                line.append(i)
            else:
                if line != []:
                    line_from = line[0]
                    line_to = line[len(line)-1]
                    for j in xrange(cols):
                        white = cv2.countNonZero(self.image[line_from:line_to, j])
                        if white != 0:
                            word.append(j)
                            if space != []:
                                newspace = [space[0], space[len(space)-1]]
                                spaces.append(newspace)
                                space = []
                        else:
                            space.append(j)
                            if word != []:
                                newword = [word[0], word[len(word)-1]]
                                words.append(newword)
                                letter_rects.append(Rect(x=newword[0], y=line_from, w=newword[1], h=line_to))
                                word = []
                    start = spaces[0][1]
                    end = words[-1][1]
                    spaces = spaces[1:-1]
                    spaces_wage = sum([l[1] - l[0] for l in spaces]) / len(spaces)
                    rect = Rect(x=start, y=line[0], w=0, h=line[len(line)-1])
                    for space in spaces:
                        if space[1] - space[0] <= spaces_wage:
                            continue
                        rect.w = space[0]
                        rects.append(rect)
                        rect = Rect(x=space[1], y=line[0], w=0, h=line[len(line)-1])
                    rect.w = end
                    rects.append(rect)
                    spaces = []
                    line = []
        dst = cv2.cvtColor(self.image, cv2.COLOR_GRAY2BGR)
        i = 0
        self.letter_rects = letter_rects
        #zaznaczanie na obrazku prostokątami, segmentacja liter
        for rect in letter_rects:
            #cv2.rectangle(dst, (rect.x, rect.y), (rect.w, rect.h), (0, 0, 255), 1)
            img = self.image[rect.y-1:rect.h+1, rect.x-1:rect.w+1]
            #cv2.imwrite("im" + str(i) + ".png", img)
            i += 1
            self.segmentate(img)
        for rect in rects:
            cv2.rectangle(dst, (rect.x, rect.y), (rect.w, rect.h), (0, 255, 0), 1)
        self.dst = dst

    def draw_res(self, res):
        for i, r in enumerate(res):
            origin = (self.letter_rects[i].x, self.letter_rects[i].h)
            textColor = (0, 0, 255)
            cv2.putText(self.dst, str(r), origin,
                        cv2.FONT_HERSHEY_PLAIN, 1.5, textColor,
                        thickness=2)



def show(image, window="output"):
    if image is not None:
        cv2.imshow(window, image)
        k = cv2.waitKey(0)
        if k == 27:
            cv2.destroyAllWindows()



if __name__ == "__main__":

    menu_mode = {'t': "Training", 'r': "Recognizing"}
    menu_type = {'d': "Digits", 'l': "Letters"}
    menu_model = {'1': "cvSVM", '2': "RTrees", '3': "MLP", '4': "Bayes"}
    menu_feature = {'b': "Binary", 'h': "HOG", 'm': "Moments"}

    datatypes = ("numbers", "letters")
    types = {datatypes[0]: 10, datatypes[1]: 26}

    while True:
        training = False
        testing = False
        recognizing = False

        for m in menu_mode.keys():
            print m, menu_mode[m]
        mode = raw_input("Select mode: ")
        if mode == 't':
            training = True
        elif mode == 'r':
            recognizing = True
        else:
            print("Error")
            continue

        # for m in menu_type.keys():
        #     print m, menu_type[m]
        # type_c = raw_input("Select data type: ")
        # datatype = None
        # if type_c == 'd':
        #     datatype = datatypes[0]
        #     training_image = "digits.png"
        #     test_image = "test.png"
        #     cell_size = (20, 20)
        # elif type_c == 'l':
        #     datatype = datatypes[1]
        #     training_image = "letters.jpg"
        #     test_image = "test_letters.png"
        #     cell_size = (34, 65)
        # else:
        #     print("Error")
        #     continue
        datatype = datatypes[0]
        training_image = "digits.png"
        test_image = "test.png"
        cell_size = (20, 20)

        for m in menu_model.keys():
            print m, menu_model[m]
        model_c = raw_input("Select model: ")
        model = None
        if model_c == '1':
            model = cvSVM(C=2.67, gamma=5.383)
        elif model_c == '2':
            model = RTrees()
        elif model_c == '3':
            model = MLP(types[datatype])
        elif model_c == '4':
            model = Bayes()
        else:
            print("Error")
            continue


        for m in menu_feature.keys():
            print m, menu_feature[m]
        feature_c = raw_input("Select feature vector: ")
        feature = None
        if feature_c == 'b':
            feature = BinaryFeature()
        elif feature_c == 'h':
            feature = HOGFeature()
        elif feature_c == 'm':
            feature = MomentsFeature()
        else:
            print("Error")
            continue

        print("---------------------------------------------------------")
        data_file = datatype + "_" + menu_feature[feature_c] + "_" + menu_model[model_c] + ".dat"

        if training or testing:
            training = Training(datatype, feature)
            training.load_data(training_image, cell_size)
            training.shuffle()

            map(training.segmentate, training.data)
            map(training.feature.calculate, training.segments)

            training.feature.vector = np.float32(training.feature.vector)
            train_no = int(0.9 * len(training.feature.vector))

            training.digits_data, digits_test = np.split(training.segments, [train_no])
            training.samples_data, samples_test = np.split(training.feature.vector, [train_no])
            training.labels_data, labels_test = np.split(training.labels, [train_no])
            training.train_save(model, data_file)

        if recognizing:
            recognize = Recognizing(datatype, feature, test_image, data_file, model)
            recognize.process_input()
            map(recognize.feature.calculate, recognize.segments)
            recognize.feature.vector = np.float32(recognize.feature.vector)
            res = recognize.predict(recognize.feature.vector)
            recognize.draw_res(res)
            cv2.imshow("IMAGE", recognize.image)
            show(recognize.dst)

        if testing:
            recognize = Recognizing(datatype, feature, test_image, data_file, model)
            recognize.test(samples_test, labels_test, digits_test)
