import cv2
import numpy as np

class Model(object):
    def load(self, filename):
        self.model.load(filename)

    def save(self, filename):
        self.model.save(filename)


class cvSVM(Model):
    def __init__(self, C=1, gamma=0.5):
        self.model = cv2.SVM()
        print("Model: cvSVM")
        self.params = dict(kernel_type = cv2.SVM_RBF,
                            svm_type = cv2.SVM_C_SVC,
                            C = C,
                            gamma = gamma)

    def train(self, data, results):
        self.model.train(data, results, params = self.params)

    def predict(self, data):
        return self.model.predict_all(data).ravel()


class RTrees(Model):
    def __init__(self):
        self.model = cv2.RTrees()
        self.params = dict(max_depth = 10)
        print("Model: RTrees")

    def train(self, data, results):
        data_n, var_n = data.shape
        var_types = np.array([cv2.CV_VAR_NUMERICAL] * var_n + [cv2.CV_VAR_CATEGORICAL], np.uint8)
        self.model.train(data, cv2.CV_ROW_SAMPLE, results, varType=var_types, params=self.params)

    def predict(self, data):
        return np.float32([self.model.predict(d) for d in data])


class MLP(Model):
    def __init__(self, count):
        self.model = cv2.ANN_MLP()
        self.params = dict(term_crit = (cv2.TERM_CRITERIA_COUNT, 300, 0.01),
                           train_method = cv2.ANN_MLP_TRAIN_PARAMS_BACKPROP,
                           bp_dw_scale = 0.001,
                           bp_moment_scale = 0.0)
        self.class_n = count
        print("Model: MLP")

    def unroll_res(self, res):
        s_n = len(res)
        new_res = np.zeros(s_n * self.class_n, np.int32)
        resp_id = np.int32(res + np.arange(s_n) * self.class_n)
        new_res[resp_id] = 1
        return new_res

    def train(self, data, results):
        data_n, var_n = data.shape
        new_res = self.unroll_res(results).reshape(-1, self.class_n)
        layer_sizes = np.int32([var_n, 100, 100, self.class_n])
        self.model.create(layer_sizes)
        self.model.train(data, np.float32(new_res), None, params=self.params)

    def predict(self, data):
        ret, resp = self.model.predict(data)
        return resp.argmax(-1)


class Bayes(Model):
    def __init__(self):
        self.model = cv2.NormalBayesClassifier()

    def train(self, data, results):
        self.model.train(data, results) #update?

    def predict(self, data):
        ret, resp = self.model.predict(data)
        resp = np.float32([i for sl in resp for i in sl])
        return resp

