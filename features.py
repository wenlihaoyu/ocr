import cv2
import numpy as np
from numpy.linalg import norm

class Feature(object):
    def __init__(self):
        self.vector = []


class BinaryFeature(Feature):
    def __init__(self):
        super(BinaryFeature, self).__init__()
        print("Features: white px count")

    def calculate(self, img):
        cells = [np.hsplit(row, 4) for row in np.vsplit(img, 4)]
        cells = np.array(cells)
        cells = cells.reshape(-1, 4, 4)
        white = [cv2.countNonZero(cell) for cell in cells]
        self.vector.append(white)


class MomentsFeature(Feature):
    def __init__(self):
        super(MomentsFeature, self).__init__()
        print("Features: Moments")

    def calculate(self, img):
        moments = cv2.moments(img, 1)
        humoments = cv2.HuMoments(moments).reshape((1, 7))
        self.vector.append(humoments[0])


class HOGFeature(Feature):
    def __init__(self):
        super(HOGFeature, self).__init__()
        print("Features: HOG")

    def calculate(self, img):
        gx = cv2.Sobel(img, cv2.CV_32F, 1, 0)
        gy = cv2.Sobel(img, cv2.CV_32F, 0, 1)
        mag, ang = cv2.cartToPolar(gx, gy)
        bin_n = 16
        bin = np.int32(bin_n*ang/(2*np.pi))
        bin_cells = bin[:10,:10], bin[10:,:10], bin[:10,10:], bin[10:,10:]
        mag_cells = mag[:10,:10], mag[10:,:10], mag[:10,10:], mag[10:,10:]
        hists = [np.bincount(b.ravel(), m.ravel(), bin_n) for b, m in zip(bin_cells, mag_cells)]
        hist = np.hstack(hists)

        # transform to Hellinger kernel
        eps = 1e-7
        hist /= hist.sum() + eps
        hist = np.sqrt(hist)
        hist /= norm(hist) + eps

        self.vector.append(hist)
